//
//  StringExtension.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 11/24/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation

extension String {
    func stringToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}


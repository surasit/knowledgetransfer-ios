//
//  ExamViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 12/18/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

class ExamViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    lazy var loadingView: MaterialLoadingView = {
        let loadingIndicator = MaterialLoadingView(frame: self.view.frame)
        loadingIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(loadingIndicator)
        return loadingIndicator
    }()
    
    var contentModel: ContentModel?
    let urlStr = "https://docs.google.com/forms/d/e/1FAIpQLSc3_o6pe6EJnr9jeufxba5k9U_tV6usC7iWXt33qXidpVw5rw/viewform?usp=sf_link"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "แบบสอบถาม"
        
        loadingView.startLoading()
        
        let url = URL (string: urlStr)
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension ExamViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("WebViewDidStart")
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("WebViewDidFinish")
        loadingView.stopLoading()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Fail")
        loadingView.stopLoading()
    }
}


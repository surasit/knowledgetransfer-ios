//
//  ContentModel.swift
//
//  Created by Surasit Intawong on 7/30/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ContentModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let status = "status"
    static let topicName = "topic_name"
    static let catId = "cat_id"
    static let catName = "cat_name"
    static let contentId = "content_id"
    static let thumbnail = "thumbnail"
    static let authorId = "author_uid"
    static let authorAvatar = "author_avatar"
    static let author = "author"
    static let authorInfo = "author_info"
    static let created = "created"
    static let shared = "shared"
    static let views = "views"
    static let reviews = "reviews"
    static let objective = "objective"
  }

  // MARK: Properties
    public var descriptionValue: String?
    public var status: String?
    public var topicName: String?
    public var catId: String?
    public var catName: String?
    public var contentId: String?
    public var thumbnail: String?
    public var authorId: String?
    public var author: String?
    public var authorAvatar: String?
    public var authorInfo: String?
    public var created: String?
    public var shared: Int?
    public var views: Int?
    public var reviews: String?
    public var objective: String?

  // MARK: SwiftyJSON Initializers
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  public init(json: JSON) {
    descriptionValue = json[SerializationKeys.descriptionValue].string
    status = json[SerializationKeys.status].string
    topicName = json[SerializationKeys.topicName].string
    catId = json[SerializationKeys.catId].string
    catName = json[SerializationKeys.catName].string
    contentId = json[SerializationKeys.contentId].string
    thumbnail = json[SerializationKeys.thumbnail].string
    authorId = json[SerializationKeys.authorId].string
    author = json[SerializationKeys.author].string
    authorAvatar = json[SerializationKeys.authorAvatar].string
    authorInfo = json[SerializationKeys.authorInfo].string
    created = json[SerializationKeys.created].string
    shared = json[SerializationKeys.shared].intValue
    views = json[SerializationKeys.views].intValue
    reviews = json[SerializationKeys.reviews].string
    objective = json[SerializationKeys.objective].string
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = topicName { dictionary[SerializationKeys.topicName] = value }
    if let value = catId { dictionary[SerializationKeys.catId] = value }
    if let value = catName { dictionary[SerializationKeys.catName] = value }
    if let value = contentId { dictionary[SerializationKeys.contentId] = value }
    if let value = thumbnail { dictionary[SerializationKeys.thumbnail] = value }
    if let value = authorId { dictionary[SerializationKeys.authorId] = value }
    if let value = author { dictionary[SerializationKeys.author] = value }
    if let value = authorAvatar { dictionary[SerializationKeys.authorAvatar] = value }
    if let value = authorInfo { dictionary[SerializationKeys.authorInfo] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = shared { dictionary[SerializationKeys.shared] = value }
    if let value = views { dictionary[SerializationKeys.views] = value }
    if let value = reviews { dictionary[SerializationKeys.reviews] = value }
    if let value = objective { dictionary[SerializationKeys.objective] = value }
    return dictionary
  }

}

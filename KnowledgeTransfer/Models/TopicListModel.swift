//
//  TopicListModel.swift
//
//  Created by Surasit Intawong on 6/24/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class TopicListModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let status = "status"
    static let topicName = "topic_name"
    static let catId = "cat_id"
    static let catName = "cat_name"
    static let contentId = "content_id"
    static let thumbnail = "thumbnail"
  }

  // MARK: Properties
  public var descriptionValue: String?
  public var status: String?
  public var topicName: String?
  public var catId: String?
  public var catName: String?
  public var contentId: String?
  public var thumbnail: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    descriptionValue = json[SerializationKeys.descriptionValue].string
    status = json[SerializationKeys.status].string
    topicName = json[SerializationKeys.topicName].string
    catId = json[SerializationKeys.catId].string
    catName = json[SerializationKeys.catName].string
    contentId = json[SerializationKeys.contentId].string
    thumbnail = json[SerializationKeys.thumbnail].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = topicName { dictionary[SerializationKeys.topicName] = value }
    if let value = catId { dictionary[SerializationKeys.catId] = value }
    if let value = catName { dictionary[SerializationKeys.catName] = value }
    if let value = contentId { dictionary[SerializationKeys.contentId] = value }
    if let value = thumbnail { dictionary[SerializationKeys.thumbnail] = value }
    return dictionary
  }

}

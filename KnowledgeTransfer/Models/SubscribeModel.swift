//
//  SubscribeModel.swift
//
//  Created by Surasit Intawong on 11/12/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class SubscribeModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SubscribeKeys {
    static let fcmToken = "fcm_token"
    static let status = "status"
    static let userId = "userid"
  }

  // MARK: Properties
    public var fcmToken: String?
    public var status: Bool? = false
    public var userId: String?

  // MARK: SwiftyJSON Initializers
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  public required init(json: JSON) {
    fcmToken = json[SubscribeKeys.fcmToken].string
    status = json[SubscribeKeys.status].boolValue
    userId = json[SubscribeKeys.userId].string
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fcmToken { dictionary[SubscribeKeys.fcmToken] = value }
    dictionary[SubscribeKeys.status] = status
    if let value = userId { dictionary[SubscribeKeys.userId] = value }
    return dictionary
  }

}

//
//  NotificationRequest.swift
//
//  Created by Surasit Intawong on 11/19/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct NotificationRequest {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct NotificationRequestKeys {
    static let notification = "notification"
    static let condition = "condition"
    static let priority = "priority"
  }

  // MARK: Properties
  public var notification: Notification?
  public var condition: String?
  public var priority: String?

  // MARK: SwiftyJSON Initializers
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  public init(json: JSON) {
    notification = Notification(json: json[NotificationRequestKeys.notification])
    condition = json[NotificationRequestKeys.condition].string
    priority = json[NotificationRequestKeys.priority].string
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = notification { dictionary[NotificationRequestKeys.notification] = value.dictionaryRepresentation() }
    if let value = condition { dictionary[NotificationRequestKeys.condition] = value }
    if let value = priority { dictionary[NotificationRequestKeys.priority] = value }
    return dictionary
  }

}

public struct Notification {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct NotificationBodyKeys {
        static let body = "body"
        static let title = "title"
    }
    
    // MARK: Properties
    public var body: String?
    public var title: String?
    
    // MARK: SwiftyJSON Initializers
    public init(object: Any) {
        self.init(json: JSON(object))
    }

    public init(json: JSON) {
        body = json[NotificationBodyKeys.body].string
        title = json[NotificationBodyKeys.title].string
    }
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = body { dictionary[NotificationBodyKeys.body] = value }
        if let value = title { dictionary[NotificationBodyKeys.title] = value }
        return dictionary
    }
    
}

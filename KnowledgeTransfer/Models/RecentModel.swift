//
//  RecentModel.swift
//
//  Created by Surasit Intawong on 10/21/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RecentModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct RecentModelKeys {
    static let topicName = "topic_name"
    static let thumbnail = "thumbnail"
    static let contentId = "content_id"
    static let lastUpdate = "last_update"
    static let authorUid = "author_uid"
    static let shareBy = "share_by"
    static let catId = "cat_id"
    static let catName = "cat_name"
  }

  // MARK: Properties
    public var topicName: String?
    public var thumbnail: String?
    public var contentId: String?
    public var lastUpdate: String?
    public var authorUid: String?
    public var shareBy: String?
    public var catId: String?
    public var catName: String?

  // MARK: SwiftyJSON Initializers
    init() {
        
    }
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }

  public required init(json: JSON) {
    topicName = json[RecentModelKeys.topicName].string
    thumbnail = json[RecentModelKeys.thumbnail].string
    contentId = json[RecentModelKeys.contentId].string
    lastUpdate = json[RecentModelKeys.lastUpdate].string
    authorUid = json[RecentModelKeys.authorUid].string
    shareBy = json[RecentModelKeys.shareBy].string
    catId = json[RecentModelKeys.catId].string
    catName = json[RecentModelKeys.catName].string
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = topicName { dictionary[RecentModelKeys.topicName] = value }
    if let value = thumbnail { dictionary[RecentModelKeys.thumbnail] = value }
    if let value = contentId { dictionary[RecentModelKeys.contentId] = value }
    if let value = lastUpdate { dictionary[RecentModelKeys.lastUpdate] = value }
    if let value = authorUid { dictionary[RecentModelKeys.authorUid] = value }
    if let value = shareBy { dictionary[RecentModelKeys.shareBy] = value }
    if let value = catId { dictionary[RecentModelKeys.catId] = value }
    if let value = catName { dictionary[RecentModelKeys.catName] = value }
    
    return dictionary
  }

}

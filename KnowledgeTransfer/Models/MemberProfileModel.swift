//
//  MemberProfileModel.swift
//
//  Created by Surasit Intawong on 10/7/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class MemberProfileModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
private struct MemberProfileKeys {
    static let fullname = "fullname"
    static let userid = "userid"
    static let password = "password"
    static let username = "username"
    static let avatar = "avatar"
    static let fcmToken = "fcm_token"
}

    // MARK: Properties
    public var fullname: String?
    public var userid: String?
    public var password: String?
    public var username: String?
    public var avatar: String?
    public var fcmToken: String?

  // MARK: SwiftyJSON Initializers
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  public required init(json: JSON) {
    fullname = json[MemberProfileKeys.fullname].string
    userid = json[MemberProfileKeys.userid].string
    password = json[MemberProfileKeys.password].string
    username = json[MemberProfileKeys.username].string
    avatar = json[MemberProfileKeys.avatar].string
    fcmToken = json[MemberProfileKeys.fcmToken].string
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fullname { dictionary[MemberProfileKeys.fullname] = value }
    if let value = userid { dictionary[MemberProfileKeys.userid] = value }
    if let value = password { dictionary[MemberProfileKeys.password] = value }
    if let value = username { dictionary[MemberProfileKeys.username] = value }
    if let value = avatar { dictionary[MemberProfileKeys.avatar] = value }
    if let value = fcmToken { dictionary[MemberProfileKeys.fcmToken] = value }
    return dictionary
  }

}

//
//  Date.swift
//  V1
//
//  Created by Kaweerut Kanthawong on 4/3/2560 BE.
//  Copyright © 2560 Ascend Money Thailand. All rights reserved.
//

import Foundation

extension Date {
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Resuslt
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func toString(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func toString(with style: DateFormatter.Style = .long) -> String {
        let dateFormatter = DateFormatter()
        let buddhistCalendar = Calendar(identifier: .buddhist)
        dateFormatter.locale = Locale(identifier: "th-TH")
        dateFormatter.calendar = buddhistCalendar
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: self)
    }
    
    func isBetween(hour h1: Int, to h2: Int) -> Bool {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self)
        
        if h1 <= hour, hour < h2 {
            return true
        }
        
        return false
    }
    
    static func formatter(_ format: String? = nil) -> DateFormatter {
        let dateFormatter = DateFormatter()
        let buddhistCalendar = Calendar(identifier: .buddhist)
        dateFormatter.locale = Locale(identifier: "th-TH")
        dateFormatter.calendar = buddhistCalendar
        if format != nil {
            dateFormatter.dateFormat = format
        }
        return dateFormatter
    }
    
    func fromTime(time: String) -> Date? {
        let dateFormatter = Date.formatter("yyyy-MM-dd")
        let fromString = dateFormatter.string(from: self) + " " + time.trimmed
        dateFormatter .dateFormat = "yyyy-MM-dd HH:mm"
        guard let date = dateFormatter.date(from: fromString) else {
            return nil
        }
        return date
    }
    
    func isBetween(d1: Date, d2: Date) -> Bool {
        if d1.isLessThanDate(dateToCompare: self), self.isLessThanDate(dateToCompare: d2) {
            return true
        }
        
        return false
    }
    
}

//
//  IntExtension.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 10/8/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
extension Int {
    func randomIntFrom(start: Int, to end: Int) -> Int {
        var a = start
        var b = end
        // swap to prevent negative integer crashes
        if a > b {
            swap(&a, &b)
        }
        return Int(arc4random_uniform(UInt32(b - a + 1))) + a
    }
}

//
//  UIColorExtension.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 6/2/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1.0)
    }
}

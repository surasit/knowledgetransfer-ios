//
//  UIViewControllerExtensoin.swift
//  V1
//
//  Created by ieofong on 2/17/2560 BE.
//  Copyright © 2560 Code BanBan. All rights reserved.
//

import Foundation
import UIKit

//MARK: Extension
extension UIViewController {
    
    func popViewControllerAction(sender:AnyObject){
        self.view.resignFirstResponder()
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
    }

}

//
//  StringExtension.swift
//  V1
//
//  Created by ieofong on 2/15/2560 BE.
//  Copyright © 2560 Code BanBan. All rights reserved.
//

import Foundation

extension String {

    var length: Int {
        return self.utf16.count
    }
    
    var isNumber : Bool {
        get {
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }

    var localized: String {

        let defaults = Foundation.UserDefaults.standard

        if let lang = defaults.string(forKey: "currentlanguage") {
            return NSLocalizedString(self, tableName: lang, bundle: Bundle.main, value: "", comment: "")
        }
        else {
            return NSLocalizedString(self, tableName: "th", bundle: Bundle.main, value: "", comment: "")
        }
    }
    
    var isURL : Bool {
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: self)
    }

    var isAlphabet: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }

    func convertDateFormatter(from date: String, to newDate: String) -> String {
        var timeStamp = ""

        let defaults = Foundation.UserDefaults.standard
        let lang = defaults.string(forKey: "currentlanguage") ?? "th"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = date
        dateFormatter.timeZone = NSTimeZone(abbreviation: "ICT") as TimeZone!
        if let ndate = dateFormatter.date(from: self)
        {
            dateFormatter.dateFormat = newDate
            dateFormatter.timeZone = NSTimeZone(abbreviation: "ICT") as TimeZone!
            dateFormatter.locale =  NSLocale(localeIdentifier: lang) as Locale!
            timeStamp = dateFormatter.string(from: ndate)
        }

        return timeStamp

    }

    func date(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "ICT") as TimeZone!
        let date = dateFormatter.date(from: self)
        return date
    }

    func formatCurrency(digits: Int) -> String {

//        let number = NSDecimalNumber(string: self)
        let number = Decimal(string: self)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = digits
        formatter.currencySymbol = ""
        
        guard let num = number as? NSDecimalNumber else {
            return "0.0"
        }
        
        let result = formatter.string(from: num.doubleValue as NSNumber)
        let trimmedResult = result!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedResult
    }

    subscript(r: Range<Int>) -> String? {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[Range(start ..< end)]
    }

    func countInstances(of stringToFind: String) -> Int {
        var stringToSearch = self
        var count = 0
        repeat {
            guard let foundRange = stringToSearch.range(of: stringToFind, options: .diacriticInsensitive)
                else { break }
            stringToSearch = stringToSearch.replacingCharacters(in: foundRange, with: "")
            count += 1

        } while (true)

        return count
    }

    func safelyLimitedTo(length n: Int)->String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }

    func isStingMaxLength(str:String, maxLength:Int)->Bool {
        if str.length == maxLength {
            return true
        }
        return false
    }
    
    func toDateTime() -> Date
    {
        //Create Date Formatter
        let dateFormatter = DateFormatter()
        
        //Specify Format of String to Parse
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        dateFormatter.locale = Locale(identifier: "en_US")
        
        
        //Parse into NSDate
        let dateFromString: Date = dateFormatter.date(from: self) ?? Date()
        
        //Return Parsed Date
        return dateFromString
    }
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func year2DigitsFormatter() -> String? {
        let dateString = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        dateFormatter.locale = Locale.init(identifier: "en_US")
        
        guard let dateObj = dateFormatter.date(from: dateString) else {
            return self
        }
        
        dateFormatter.dateFormat = "yy"
        let dateResult = dateFormatter.string(from: dateObj)
        
        return dateResult
    }
    
    func month2DigitsFormatter() -> String? {
        let dateString = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm"
        dateFormatter.locale = Locale.init(identifier: "en_US")
        
        guard let dateObj = dateFormatter.date(from: dateString) else {
            return self
        }
        
        dateFormatter.dateFormat = "mm"
        let dateResult = dateFormatter.string(from: dateObj)
        
        return dateResult
    }
    
    func toBool() -> Bool {
        let strBool = self.lowercased()
        switch strBool {
        case "true", "yes", "y", "1":
            return true
        case "false", "no", "n", "0":
            return false
        default:
            return false
        }
    }
    
    
    func stringToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

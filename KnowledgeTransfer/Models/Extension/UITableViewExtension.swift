//
//  UITableViewExtension.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 10/4/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func setTableHeaderViewStatus(in tableView:UITableView, responseView: ErrorResponseView?) {
        tableView.isHidden = false
        guard let view = responseView else {
            tableView.tableHeaderView = nil
            return
        }
        tableView.tableHeaderView = view
        tableView.reloadData()
    }
}

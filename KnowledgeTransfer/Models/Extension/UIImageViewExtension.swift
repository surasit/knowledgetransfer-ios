//
//  UIImageViewExtension.swift
//  V1
//
//  Created by Kittikorn Ariyasuk on 9/26/2560 BE.
//  Copyright © 2560 Ascend Money Thailand. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func addBlurEffectFilter() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    func imageBlurEffect(withImage image: UIImage?) {

        if let img = image, let cgImg = img.cgImage {
            self.backgroundColor = UIColor.clear
            let inputImage = CIImage(cgImage: cgImg)
            let filter = CIFilter(name: "CIGaussianBlur")
            filter?.setValue(inputImage, forKey: "inputImage")
            filter?.setValue(10, forKey: "inputRadius")
            let blurred = filter?.outputImage
            
            var newImageSize: CGRect = (blurred?.extent)!
            newImageSize.origin.x += (newImageSize.size.width - (img.size.width)) / 2
            newImageSize.origin.y += (newImageSize.size.height - (img.size.height)) / 2
            newImageSize.size = img.size
            
            let resultImage: CIImage = filter?.value(forKey: "outputImage") as! CIImage
            let context: CIContext = CIContext(options: nil)
            let cgimg: CGImage = context.createCGImage(resultImage, from: newImageSize)!
            let blurredImage: UIImage = UIImage(cgImage: cgimg)
            self.image = blurredImage
        }
    }
}

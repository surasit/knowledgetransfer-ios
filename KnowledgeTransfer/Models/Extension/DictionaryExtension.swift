//
//  DictionaryExtension.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 11/19/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation

extension Dictionary {
    
    var jsonString: String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? ""
        } catch {
            return ""
        }
    }
    
    var queryString: String? {
        let queryStr: String? = self.reduce("") { "\($0!)\($1.0)=\($1.1)&" }
        return queryStr?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
    
}

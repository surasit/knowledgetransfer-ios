//
//  ConnectionManager.swift
//  V1
//
//  Created by Somjintana K. on 22/03/2017.
//  Copyright © 2017 Ascend Money Thailand. All rights reserved.
//

import UIKit
import Alamofire

class ConnectionManager: NSObject {
    
    static private let logDescription = true
    static private let manager = Alamofire.SessionManager.default
    static private var sslPinningManagers: [String: Alamofire.SessionManager] = [:]
    static private let defaultRequestTimeout: TimeInterval = 45

    static func GET(_ url: String, params: [String: Any]?, headers: [String: String]?, timeout: TimeInterval? = nil, encode: ParameterEncoding = URLEncoding.default, callback: @escaping ((_ dataResponse: DataResponse<Any>) -> Void)) {
        
        if logDescription {
            print("[Request] \(url)\n[Params] \(String(describing: params))\n[Method] GET")
        }
        
        let sessionManager = manager
        
        sessionManager.session.configuration.timeoutIntervalForRequest = timeout ?? defaultRequestTimeout
        
        sessionManager.request(url, method: .get, parameters: params, encoding: encode, headers: headers)
            .responseJSON { (response) in
                if logDescription {
                    print("[Success] \(response.result.isSuccess)")
                    print("[Response] \(String(describing: response.result.value))")
                }
                
                callback(response)
        }
    }
    
    static func POST(_ url: String, params: [String: Any]?, headers: [String: String]?, timeout: TimeInterval? = nil, encode: ParameterEncoding = URLEncoding.default, callback: @escaping ((_ dataResponse: DataResponse<Any>) -> Void)) {
        
        if logDescription {
            print("[Request] \(url)\n[Params] \(String(describing: params))\n[Method] POST")
        }
        
        let sessionManager = manager
        
        sessionManager.session.configuration.timeoutIntervalForRequest = timeout ?? defaultRequestTimeout
        
        sessionManager.request(url, method: .post, parameters: params, encoding: encode, headers: headers)
            .responseJSON { (response) in
                if logDescription {
                    print("[Success] \(response.result.isSuccess)")
                    print("[Response] \(String(describing: response.result.value))")
                }
                
                DispatchQueue.main.async {
                    callback(response)
                }
        }
    }
}

extension String: ParameterEncoding {
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}

//
//  MemberProfileManager.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 10/7/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import Firebase

class MemberProfileManager: NSObject {
    var ref: DatabaseReference!
    
    override init() {
        ref = Database.database().reference()
    }
    
    func getContents(withKey key:String, equalToValue value: String, completion: @escaping (_ response: MemberProfileModel) -> Void) {
        let queryData = ref.child(ContextKey.members.rawValue).queryOrdered(byChild: key).queryEqual(toValue: value)
        
        queryData.observe(.value, with: { (snapshot) in
            for snap in snapshot.children {
                if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                    let contentModel = MemberProfileModel(object: data)
                    completion(contentModel)
                }
            }
        })
    }
}

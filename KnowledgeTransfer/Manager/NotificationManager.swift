//
//  NotificationManager.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 11/19/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import Firebase

class NotificationManager: NSObject {
    var ref: DatabaseReference!
    
    let authorizationKey = "AIzaSyCOW0uMNbEFBB5tu8d2poCvTaBGhnMxMOw"
    let notificationURL = "https://fcm.googleapis.com/fcm/send"
    
    override init() {
        ref = Database.database().reference()
    }
    
    func send(withTopic contentModel: ContentModel?) {
        guard let content = contentModel else {
            print("[NotificationManager] >>> Can't send notification with topic")
            return
        }
        
        let header: [String: String] = [
            HTTP_CONTENTTYPE_FIELD: HTTP_CONTENTTYPE_VALUE,
            "Authorization": "key=\(authorizationKey)"
        ]
        
        let params: [String: Any] = [
            "condition": "'\(content.catId ?? "")' in topics",
            "priority": "high",
            "notification": ["sound" : "default", "body": content.topicName ?? "", "title": "Found new topics subscribed", "content": content.dictionaryRepresentation().jsonString]
        ]

        ConnectionManager.POST(notificationURL, params: nil, headers: header, encode: params.jsonString) { (response) in
            print("Sending ......")
            print(response)
        }
    }
    
    func saveNotificationList(withContent contentModel: ContentModel) {
        let contentID = contentModel.contentId ?? ""
        print(contentID)
        let refChild = ref.child(ContextKey.notification.rawValue).child(contentID)
        refChild.updateChildValues(contentModel.dictionaryRepresentation())
    }
    
//    func saveNotificationListDict(withContent content: [String: AnyObject) {
//        let refChild = ref.child(ContextKey.notification.rawValue).child(contentID)
//        refChild.updateChildValues(contentModel.dictionaryRepresentation())
//    }
}

//
//  FirebaseManager.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 8/27/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON

enum FireBaseError: Error {
    case Empty
}

class FirebaseManager: NSObject {
    var childContext = "CONTENTS"
    let recentContext = "RECENTS"
    var ref: DatabaseReference!
    
    override init() {
        ref = Database.database().reference()
    }
    
    func getData(withKey key:String, equalToValue value: String, completion: @escaping (_ response: Any) -> Void) {
        ref.child(ContextKey.categories.rawValue).queryOrdered(byChild: key).observe(.value, with: { snapshot in
            if let dataSnapshot = snapshot.value as? [String : AnyObject], let data = dataSnapshot[key] {
                if value != "", let result = data[value] {
                    completion(result as Any)
                }
            }
        })
    }
    
    func getContents(withKey key:String, equalToValue value: String, completion: @escaping (_ response: [ContentModel]) -> Void) {
        
        let queryData = ref.child(childContext).queryOrdered(byChild: key).queryEqual(toValue: value)
        
        queryData.observe(.value, with: { (snapshot) in
            var result: [ContentModel] = []
            for snap in snapshot.children {
                if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                    let contentModel = ContentModel(object: data)
                    result.append(contentModel)
                }
                completion(result)
            }
            
        })
    }
    
    func getAllContents(completion: @escaping (_ response: [ContentModel]?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let queryData = ref.child(ContextKey.contents.rawValue)
        
        queryData.observe(.value, with: { (snapshot) in
            var result: [ContentModel]? = []
            if snapshot.exists() {
                for snap in snapshot.children {
                    if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                        let contentModel = ContentModel(object: data)
                        result?.append(contentModel)
                    }
                    completion(result)
                }
            }else{
                failure(FireBaseError.Empty)
            }
        }) { (error) in
            failure(error)
        }

    }
    
    func getAllNotification(completion: @escaping (_ response: [ContentModel]?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let queryData = ref.child(ContextKey.notification.rawValue)
        
        queryData.observe(.value, with: { (snapshot) in
            var result: [ContentModel]? = []
            if snapshot.exists() {
                for snap in snapshot.children {
                    if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                        let contentModel = ContentModel(object: data)
//                        let currentUserId = Auth.auth().currentUser?.uid ?? ""
//                        self.getData(withKey: contentModel.catId ?? "", equalToValue: currentUserId, completion: { (categoryResponse) in
//                            let subscribeModel = SubscribeModel(object: categoryResponse)
//                            if subscribeModel.status! {
//                                print(subscribeModel.dictionaryRepresentation())
//                                result?.append(contentModel)
//                            }
//                        })
                        result?.append(contentModel)
                    }
                    completion(result)
                }
            }else{
                failure(FireBaseError.Empty)
            }
        }) { (error) in
            failure(error)
        }
        
    }
    
    func addViewCounter(withContent content: ContentModel, completion: @escaping (_ statu: Bool) -> Void) {
        var viewCounter: Int = 1
        guard let contentID = content.contentId else {
            completion(false)
            return
        }
        
        if let views = content.views {
            viewCounter = Int(views)
            viewCounter += 1
        }
        
        let refChild = ref.child(childContext).child(contentID)
        refChild.updateChildValues(["views":viewCounter])
        completion(true)
    }
    
    func addShareCounter(withContent content: ContentModel, completion: @escaping (_ statu: Bool) -> Void) {
        var shareCounter: Int = 1
        guard let contentID = content.contentId else {
            completion(false)
            return
        }
        
        if let views = content.shared {
            shareCounter = Int(views)
            shareCounter += 1
        }
        
        let refChild = ref.child(childContext).child(contentID)
        refChild.updateChildValues(["shared":shareCounter])
        
        addRecentHistory(withContent: content)
        
        completion(true)
    }
    
    func addRecentHistory(withContent content: ContentModel) {
        let userId = content.authorId ?? ""
        let contentID = content.contentId ?? ""
        let timestamp = Date().toString(with: defaultDatePattern)

        
        let recentModel = RecentModel()
        recentModel.authorUid = userId
        recentModel.contentId = contentID
        recentModel.thumbnail = content.thumbnail ?? ""
        recentModel.topicName = content.topicName ?? ""
        recentModel.lastUpdate = timestamp
        recentModel.catId = content.catId ?? ""
        recentModel.catName = content.catName ?? ""
        
        var childID = "\(contentID)\(userId)"
        if let currentUserId = Auth.auth().currentUser?.uid {
            recentModel.shareBy = currentUserId
            childID = "\(contentID)\(currentUserId)"
        }

        let refChild = ref.child(recentContext).child(childID)
        refChild.updateChildValues(recentModel.dictionaryRepresentation())
    }
    
    func getAllRecents(withKey key:String? = nil, equalToValue value: String? = nil, completion: @escaping (_ response: [RecentModel]?) -> Void) {
        
        if let fieldName = key, let query = value {
            let queryData = ref.child(recentContext).queryOrdered(byChild: fieldName).queryEqual(toValue: query)
                queryData.observe(.value, with: { (snapshot) in
                    var result: [RecentModel]? = []
                    for snap in snapshot.children {
                        if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                            let recentModel = RecentModel(object: data)
                            result?.append(recentModel)
                        }
                        completion(result)
                    }
                    
                })
        }else{
            let queryData = ref.child(recentContext)
            queryData.observe(.value, with: { (snapshot) in
                var result: [RecentModel]? = []
                for snap in snapshot.children {
                    if let dataSnapshot = snap as? DataSnapshot, let data = dataSnapshot.value {
                        let recentModel = RecentModel(object: data)
                        result?.append(recentModel)
                    }
                    completion(result)
                }
                
            })
        }
    }
    
    func addNotificationToken(withToken token: String) {
        if let currentUserId = Auth.auth().currentUser?.uid {
            let refChild = ref.child(ContextKey.members.rawValue).child(currentUserId)
            let userToken = ["fcm_token": token]
            refChild.updateChildValues(userToken)
        }
    }
    
    func addReview(withContent content: ContentModel, andScore score: Double) {
        
        let reviewStr = content.reviews ?? ""
        var reviewArr: [Double] = []
        if let dataFromString = reviewStr.data(using: .utf8, allowLossyConversion: false) {
            if let jsonObject = JSON(data: dataFromString).arrayObject, let jsonScore = jsonObject as? [Double] {
                reviewArr = jsonScore
            }
        }
        
        reviewArr.append(score + 1.0)
        let jsonString = JSON(reviewArr)
        let reviewResponse = jsonString.rawString() ?? ""
        
        
        let refChild = ref.child(ContextKey.contents.rawValue).child(content.contentId ?? "")
        let reviewed = ["reviews": reviewResponse.replacingOccurrences(of: "\n", with: "")]
        refChild.updateChildValues(reviewed)
    }
    
    func addSubscribeCategory(withCategory categoryID: String, status: Bool = true) {
        if let currentUserId = Auth.auth().currentUser?.uid {
            let token = Messaging.messaging().fcmToken ?? ""
            let userIdAndToken = [currentUserId: ["userid":currentUserId, "fcm_token":token, "status": status]]
            
            let refChild = ref.child(ContextKey.categories.rawValue).child(categoryID)
            refChild.updateChildValues(userIdAndToken)
            
            //Subscribe Topic by category
            if status {
                Messaging.messaging().subscribe(toTopic: categoryID)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: categoryID)
            }

        }
    }
}

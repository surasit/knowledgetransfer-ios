//
//  StorageManager.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 10/8/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import Firebase

enum StorageBucketName {
    case content
    case avatar
    case defaultAvatar
    case none
}

class StorageManager: NSObject {
    let storage = Storage.storage()
    var path: String = ""
    var fileName: String = ""
 
    init(withBucketName bucketName: StorageBucketName) {
        
        switch bucketName {
        case .content:
            let timeInterval = DateTimeFormatter().getTimeInterval()
            path = "content/\(timeInterval)"
            fileName = "filename_\(timeInterval)"
        case .defaultAvatar:
            let start = 1
            let randomNumber = start.randomIntFrom(start: start, to: 10)
            path = "avatar/\(randomNumber).png"
            fileName = "\(randomNumber)"
        case .avatar:
            let uid = Auth.auth().currentUser?.uid
            path = "avatar/\(uid).png"
            fileName = "\(uid)"
        default:
            print("No setup your bucket name")
        }
        
        // Points to the root reference
//        storageRef = storage.reference(withPath: <#T##String#>)
        
//        // Points to "images"
//        let imagesRef = storageRef.child("images")
//
//        // Fetch the download URL
//        starsRef.downloadURL { url, error in
//            if let error = error {
//                // Handle any errors
//            } else {
//                // Get the download URL for 'images/stars.jpg'
//            }
//        }
    }
    
    func loadAvatarImage(completion: @escaping (_ imageURL: URL?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        // Create a reference to the file you want to download
        let storageRef = storage.reference(withPath: path)
        
        // Fetch the download URL
        storageRef.downloadURL { url, error in
            if let error = error {
                // Handle any errors
                print("Download URL Error : \(error)")
                failure(error)
            } else {
                // Get the download URL for 'images/stars.jpg'
                if let urlImage = url {
                    print("URL IMAGE : \(urlImage)")
                    completion(urlImage)
                }else{
                    print("URL Image Not Found")
                }
                
            }
        }
        
    }
    
    func uploadContentImage(imageData: NSData, completion: @escaping (_ response: URL, _ filename: String) -> Void, failure: @escaping (_ error: Error) -> Void) {
        
        let storageRef = storage.reference(withPath: path)
        let uploadMetaData = StorageMetadata()
        uploadMetaData.contentType = "image/jpeg"
        storageRef.putData(imageData as Data, metadata: uploadMetaData) { (metadata, error) in
            if let error = error {
                failure(error)
            }else{
                guard let metaDataInfo = metadata, let downloadURL = metaDataInfo.downloadURL() else {
                    return
                }
                print("Upload complete metadata >>  \(metaDataInfo)")
                print("Download URL : \(downloadURL)")

                completion(downloadURL, self.fileName)
            }
        }
    }
}

//
//  ContentManager.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 7/30/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation
import Firebase


class ContentManager: NSObject {
    var ref: DatabaseReference!
    
    override init() {
        ref = Database.database().reference()
    }
    
    func createGroupName(groupName:String, success: @escaping (_ response: String?) -> Void) {
        //Add allow user id access into this group
        
        let groupRef = self.ref.child(ContextKey.categories.rawValue)

        groupRef.observeSingleEvent(of: .value, with: { (snapshot) in

            let groupChild = groupRef.childByAutoId()
            var groupID = groupChild.key
            
            guard let groupLists = snapshot.value as? [String:AnyObject] else {
                let groupInfo = ["id":groupID,"name":groupName]
                groupChild.setValue(groupInfo)

                return
            }
            
            for (_,groupValue) in groupLists {
                if let gName = groupValue["name"] as? String, let gID = groupValue["id"] as? String, gName == groupName {
                    print(gID)
                    groupID = gID
                    
                    break
                }
            }

            let groupInfo = ["id":groupID,"name":groupName]
            groupRef.child(groupID).updateChildValues(groupInfo)
            success(groupID)
        })

    }
    
    func writeContent(params:ContentModel, atPath path:String, success: @escaping (_ response: ContentModel) -> Void, failure: @escaping (_ error: Error) -> Void) {
        
        guard let categoryName = params.catName else {
            return
        }
        
//        let categoryID = self.createGroupName(groupName: categoryName)
        self.createGroupName(groupName: categoryName) { (categoryID) in
            if let catID = categoryID {
                //Create Content
                let contentPath = self.ref.child(path)
                let contentID = contentPath.childByAutoId()
                
                //Setup content data model
                params.contentId = contentID.key
                params.catId = catID
                params.status = ContentStatus.pub.rawValue
                
                //Add Topic name then can show list on home page
                contentID.setValue(params.dictionaryRepresentation())
                
                success(params)
            }
        }
        
        
        
    }
}

//
//  CategoriesViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 5/30/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class CategoriesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let reuseCategorySubscribeIdentifier = "CellCategories"
    let reuseCategorySubscribedIdentifier = "CellCategoriesSubscribed"
    
    var ref: DatabaseReference!
    var jsonData: JSON? = nil {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    let currentUserId: String = {
        Auth.auth().currentUser?.uid ?? ""
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
//        self.readContent()
        
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.rowHeight = UITableViewAutomaticDimension

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.readContent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension CategoriesViewController {
    func readContent() {
        guard let _ = Auth.auth().currentUser?.uid else {
            return
        }
        
        let contentRef = self.ref.child(ContextKey.categories.rawValue)
        contentRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let contentLists = snapshot.value as? [String:AnyObject] else {
                return
            }

            self.jsonData = JSON(contentLists.flatMap{$0.value})
        })
        
    }

}

extension CategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseCategorySubscribeIdentifier) as? CustomTableViewCell
        
        if let json = self.jsonData, let catName = json[indexPath.row]["name"].string {
            let subscribeModel = SubscribeModel(json: json[indexPath.row][currentUserId])
            if let status = subscribeModel.status, status == true {
                cell = tableView.dequeueReusableCell(withIdentifier: reuseCategorySubscribedIdentifier) as? CustomTableViewCell
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: reuseCategorySubscribeIdentifier) as? CustomTableViewCell
            }
            
            cell?.title.text = catName
            cell?.btnFollow.tag = indexPath.row
            cell?.categoryID = json[indexPath.row]["id"].string ?? ""
        }
        
        cell?.img.tintColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
        
        return cell!
    }
}

extension CategoriesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonData != nil ? self.jsonData!.count : 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}

//
//  ReviewBoxView.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 11/23/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReviewBoxView: UIView {

    @IBOutlet weak var reviewScore: UILabel!
    @IBOutlet weak var btnReview1: UIButton!
    @IBOutlet weak var btnReview2: UIButton!
    @IBOutlet weak var btnReview3: UIButton!
    @IBOutlet weak var btnReview4: UIButton!
    @IBOutlet weak var btnReview5: UIButton!
    var btnReviewLists: [UIButton] = []
    var contentModel: ContentModel? = nil
    var reviewBoxViewController: ReviewBoxViewController?
    
    init() {
        super.init(frame: CGRect())
        fromNib()
        
        btnReviewLists = [btnReview1, btnReview2, btnReview3, btnReview4, btnReview5]
        self.setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
         btnReviewLists = [btnReview1, btnReview2, btnReview3, btnReview4, btnReview5]
        self.setupView(withFrame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        fromNib()
         btnReviewLists = [btnReview1, btnReview2, btnReview3, btnReview4, btnReview5]
        self.setupView()
    }
    
    fileprivate func setupView(withFrame frame: CGRect? = nil) {
        if let frameSize = frame {
            self.frame = frameSize
        }else{
            self.frame = CGRect(x: 0, y: 0, width: 300, height: 250)
        }
        
        self.layer.cornerRadius = 5.0
        
    }
    
    func setupStartScore(content: ContentModel) {
        let reviewStr = content.reviews ?? ""
        var reviewArr: [Double] = []
        if let dataFromString = reviewStr.data(using: .utf8, allowLossyConversion: false) {
            if let jsonObject = JSON(data: dataFromString).arrayObject, let jsonScore = jsonObject as? [Double] {
                reviewArr = jsonScore
            }
        }
        
        var score: Double = 0.0
        for review in reviewArr {
            score += review
        }
        let result = "\(score / Double(reviewArr.count))".formatCurrency(digits: 2)
        
        self.reviewScore.text = "\(result)"
    }
    
    @IBAction func didTouchButton1(_ sender: UIButton) {
        clearActiveButton()
        setupReviewButton(button: sender)
        
        if let content = self.contentModel {
            saveReviewScore(score: sender.tag, andContentModel: content)
        }
    }
    @IBAction func didTouchButton2(_ sender: UIButton) {
        clearActiveButton()
        setupReviewButton(button: sender)
        
        if let content = self.contentModel {
            saveReviewScore(score: sender.tag, andContentModel: content)
        }
    }
    @IBAction func didTouchButton3(_ sender: UIButton) {
        clearActiveButton()
        setupReviewButton(button: sender)
        
        if let content = self.contentModel {
            saveReviewScore(score: sender.tag, andContentModel: content)
        }
    }
    @IBAction func didTouchButton4(_ sender: UIButton) {
        clearActiveButton()
        setupReviewButton(button: sender)
        
        if let content = self.contentModel {
            saveReviewScore(score: sender.tag, andContentModel: content)
        }
    }
    @IBAction func didTouchButton5(_ sender: UIButton) {
        clearActiveButton()
        setupReviewButton(button: sender)
        
        if let content = self.contentModel {
            saveReviewScore(score: sender.tag, andContentModel: content)
        }
    }
    
    func clearActiveButton() {
        let reviewInActive = UIImage(named: "ic_star_border")
        for button in btnReviewLists {
            button.setImage(reviewInActive, for: .normal)
        }
    }
    
    func setupReviewButton(button: UIButton) {
        let reviewActive = UIImage(named: "ic_star")

        for buttonList in btnReviewLists {
            print(button.tag)
            print(buttonList.tag)
            if button.tag >= buttonList.tag {
                buttonList.setImage(reviewActive, for: .normal)
            }
        }
    }
    
    func saveReviewScore(score: Int, andContentModel contentModel: ContentModel) {
        print("content id : \(contentModel.contentId ?? "") >>>> score : \(score)")
        
        var scoreDouble: Double = 0.0
        scoreDouble = Double(score)
        FirebaseManager().addReview(withContent: contentModel, andScore: scoreDouble)
        
        if let vc = reviewBoxViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

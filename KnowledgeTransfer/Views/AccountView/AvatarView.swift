//
//  AvatarView.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 9/30/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase

class AvatarView: UIView {

    @IBOutlet weak var bgBlur: UIImageView!
    @IBOutlet weak var bgAvatarView: UIView!
    @IBOutlet weak var bgAvatarImg: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    init() {
        super.init(frame: CGRect())
        fromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
        
        self.frame = frame
        loadAvatarDefault()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        fromNib()
    }
    
    func loadAvatarDefault() {
        guard let userProfile = Auth.auth().currentUser else {
            return
        }
        
        self.lblName.text = userProfile.displayName
        self.lblEmail.text = userProfile.email
        
        let placeholderImg = UIImage(named: "user")
        if let avatarURL = userProfile.photoURL {
            self.bgAvatarImg.sd_setImage(with: avatarURL, placeholderImage: placeholderImg, options: .continueInBackground, progress: { (receiveSize, expectSize, targetURL) in
                print("Receive : \(receiveSize), Total : \(expectSize)")
            }, completed: { (img, error, type, url) in
                self.bgBlur.contentMode = .center
                self.bgBlur.image = img?.blur(radius: 40.0, tintColor: nil, saturationDeltaFactor: 1.8)
            })
        }
    }
    
}

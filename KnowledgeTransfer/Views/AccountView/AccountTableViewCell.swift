//
//  AccountTableViewCell.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 9/30/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }


}

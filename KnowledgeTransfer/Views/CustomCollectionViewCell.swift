//
//  CustomCollectionViewCell.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 10/14/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var bgTitleView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    func setupRecentCell(withModel recentModel: RecentModel) {
        self.lblTitle.text = recentModel.topicName
        
        self.setupThumbnail(withModel: recentModel)

    }
    
    func setupThumbnail(withModel recentModel: RecentModel) {
        guard let img = recentModel.thumbnail else {
            return
        }
        let imgURL = URL(string: img)
        self.thumbnail.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "noimage"))
    }
    
    func setBackgroundBlur() {
//        let blurEffect = UIBlurEffect(style: .dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = bgTitleView.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        bgTitleView.addSubview(blurEffectView)

        bgTitleView.backgroundColor = UIColor.black.withAlphaComponent(0.5)

    }
}

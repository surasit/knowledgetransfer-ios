//
//  HeaderView.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 6/24/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Material
import Firebase

class HeaderView: UIView {

    @IBOutlet weak var bgImage: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var authorInfo: UILabel!
    @IBOutlet weak var authorModify: UILabel!
    @IBOutlet weak var btnSubscribe: Button!
    
    var contentModel: ContentModel? = nil
    
    init() {
        super.init(frame: CGRect())
        
        fromNib()
        self.setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
        self.setupView(withFrame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        fromNib()
        self.setupView()
    }
    
    fileprivate func setupView(withFrame frame: CGRect? = nil) {
        if let frameSize = frame {
            self.frame = frameSize
        }else{
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 72)
        }
    }
    

    func setAvatar(withImageURL imgStr: String?) {
        
        let placeholderImg = UIImage(named: "noimage")
        
        
        guard let img = imgStr, img != "" , let imgURL = URL(string: img)  else {
            
            self.imgAvatar.contentMode = .scaleToFill
            self.imgAvatar.image = placeholderImg
            return
        }
        
        
        self.authorInfo.text = "Infomation"
//        self.bgImage.backgroundColor = UIColor.clear
//        self.imgAvatar.image = UIImage(named: img)
        self.imgAvatar.sd_setImage(with: imgURL, placeholderImage: placeholderImg)
    }
    
    func setupInformation(contents: ContentModel) {

        self.initSubscribeButton(contents: contents)
        
        self.authorName.text = contents.author
        self.authorInfo.text =  "Categoery : \(contents.catName ?? "")"
        self.authorModify.text = "Created : \(contents.created ?? "") "
    }
    
    //MARK: - Subscribe Action
    @IBAction func btnSubscribeDidTouch(_ sender: Button) {
        print("Subscribe did touch action")

        guard let contents = self.contentModel else {
            return
        }
        
        if let btnTitle = self.btnSubscribe.titleLabel?.text, btnTitle == "Subscribe" {
            enableSubscribe(contents: contents)
        }else{
            disableSubscribe(contents: contents)
        }
        
    }
    
    func initSubscribeButton(contents: ContentModel) {
        if let categoryID = contents.catId {
            let currentUserId = Auth.auth().currentUser?.uid ?? ""
            FirebaseManager().getData(withKey: categoryID, equalToValue: currentUserId, completion: { (response) in
                let subscribeModel = SubscribeModel(object: response)
                if subscribeModel.status!, let btnTitle = self.btnSubscribe.titleLabel?.text, btnTitle == "Subscribe" {
                    self.btnSubscribe.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.9960784314, alpha: 1)
                    self.btnSubscribe.setTitle("Subscribed", for: .normal)
                    self.btnSubscribe.setTitleColor(#colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1), for: .normal)
                    return
                }
            })
        }
    }
    
    func enableSubscribe(contents: ContentModel) {
        
        if let categoryID = contents.catId {
            self.btnSubscribe.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            self.btnSubscribe.setTitle("Subscribed", for: .normal)
            self.btnSubscribe.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            FirebaseManager().addSubscribeCategory(withCategory: categoryID, status: true)
        }
        
    }
    
    func disableSubscribe(contents: ContentModel) {
        if let categoryID = contents.catId {
            self.btnSubscribe.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.btnSubscribe.setTitle("Subscribe", for: .normal)
            self.btnSubscribe.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            FirebaseManager().addSubscribeCategory(withCategory: categoryID, status: false)
        }
    }
}

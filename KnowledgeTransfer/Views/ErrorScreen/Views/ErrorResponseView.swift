//
//  ExpressResponseView.swift
//  V1
//
//  Created by Surasit Intawong on 6/22/2560 BE.
//  Copyright © 2560 Ascend Money Thailand. All rights reserved.
//

import UIKit

class ErrorResponseView: UIView {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    init() {
        super.init(frame: CGRect())
        fromNib()
        self.setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
        self.setupView(withFrame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        fromNib()
        self.setupView()
    }
    
    fileprivate func setupView(withFrame frame: CGRect? = nil) {
        if let frameSize = frame {
            self.frame = frameSize
        }else{
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 275)
        }
        
    }

    fileprivate func setTitle(title:String?) {
        guard let strTitle = title else {
            return self.title.text = ""
        }
        
        self.title.text = strTitle
    }
    
    fileprivate func setSubTitle(subTitle:String?) {
        guard let strSubTitle = subTitle else {
            return self.subTitle.text = ""
        }
        
        self.subTitle.text = strSubTitle
    }
    
    func setViewStatus(status: HTTPStatusCodes, andTitle title: String = "", andSubTitle subTitle: String = "") {
        
        if status.isSuccess, status == .Http204_NoContent{
            self.img.image = UIImage(named: "ic-server_error")
            self.setSubTitle(subTitle: subTitle)
        }else if status.isServerError {
            self.img.image = UIImage(named: "ic-server_error")
            self.setTitle(title: "HTTP500_INTERNAL_SEVER_ERROR_TITLE".localized)
            self.setSubTitle(subTitle: "HTTP500_INTERNAL_SEVER_ERROR_MESSAGE".localized)
        }else if status.isClientError {
            self.img.image = UIImage(named: "ic-client_error")
            self.setSubTitle(subTitle: "HTTP400_BAD_REQUEST".localized)
        }
    }
    
    func needReloadDataButton() -> UIButton {
        self.retryButton.isHidden = false
        return self.retryButton
    }
    
}

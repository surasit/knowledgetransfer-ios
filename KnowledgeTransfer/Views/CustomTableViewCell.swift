//
//  CustomTableViewCell.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/18/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import SDWebImage
import Material
import Firebase
import SwiftyJSON

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var viewer: UILabel!
    @IBOutlet weak var shared: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var modify: UILabel!
    @IBOutlet weak var authorImg: UIImageView!
    @IBOutlet weak var authorView: UIView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var btnFollow: Button!
    @IBOutlet weak var reviewImg: UIImageView!
    @IBOutlet weak var reviewPoint: UILabel!
    
    @IBOutlet weak var chkView: UIView!
    
    
    var categoryID = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTopic(withTopic list:ContentModel) {
        
        self.title.text = list.topicName
        self.modify.text = "Created : \(list.created ?? "")"
        
        self.setAvatar(withImageURL: list.authorAvatar, authorName: list.author)
        self.setThumbnail(withURL: list.thumbnail)
        self.setCountViews(count: list.views)
        self.setCountShare(count: list.shared)
        self.setupReviewPoint(withTopic: list)
    }
    
    func setupReviewPoint(withTopic content:ContentModel) {
        let reviewStr = content.reviews ?? ""
        var reviewArr: [Double] = []
        if let dataFromString = reviewStr.data(using: .utf8, allowLossyConversion: false) {
            if let jsonObject = JSON(data: dataFromString).arrayObject, let jsonScore = jsonObject as? [Double] {
                reviewArr = jsonScore
            }
        }
        
        var score: Double = 0.0
        for review in reviewArr {
            score += review
        }
        let result = "\(score / Double(reviewArr.count))".formatCurrency(digits: 2)
        
        self.reviewPoint.text = "\(result)"
    }
    
    func setupNotificationTopic(withTopic list:ContentModel) {
        self.title.text = list.topicName
        self.subTitle.text = "Created : \(list.created ?? "")"
        self.setThumbnailNotification(withURL: list.thumbnail)
    }
    
    func setThumbnailNotification(withURL imgStr: String?) {
        
        guard let img = imgStr, img != "" else {
            self.img.contentMode = .scaleToFill
            self.img.image = UIImage(named:"noimage")
            return
        }
        
        let imgURL = URL(string: img)
        self.img.contentMode = .center
        self.img.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"noimage"))
        
    }
    
    func setThumbnail(withURL imgStr: String?) {

        guard let img = imgStr, img != "" else {
            self.img.contentMode = .scaleToFill
            self.img.image = UIImage(named:"noimage")
            return
        }
        
        let imgURL = URL(string: img)
        self.img.contentMode = .center
        self.img.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"noimage"))

    }
    
    func setAvatar(withImageURL imgStr: String?, authorName: String?) {

        let placeholderImg = UIImage(named: "noimage")
        self.author.text = authorName
        
        guard let img = imgStr, img != "" , let imgURL = URL(string: img) else {
            
            self.authorImg.contentMode = .scaleToFill
            self.authorImg.image = placeholderImg
//            if let name = authorName {
//                let firstChars = String(name.characters.prefix(1))
//                self.authorLabel.text = firstChars.uppercased()
//                self.author.text = name
//            }
            
            return
        }
        
        //self.authorView.backgroundColor = UIColor.clear
        self.authorImg.sd_setImage(with: imgURL, placeholderImage: placeholderImg)
    }
    
    func setCountViews(count:Int?) {
        if let counter = count {
            self.viewer.text = "Views : \(counter)"
            return
        }
        
        self.viewer.text = "Views : -"
    }

    func setCountShare(count:Int?) {
        if let counter = count {
            self.shared.text = "Shared : \(counter)"
            return
        }
        
        self.shared.text = "Shared : -"
    }
    
    //MARK: - Subscribe or follow with topic
    @IBAction func btnFollowDidTouch(_ sender: Button) {
        print("Follower did touch action")
        print(sender.tag)
        if let btnTitle = self.btnFollow.titleLabel?.text, btnTitle == "Subscribe" {
            enableSubscribe()
        }else{
            disableSubscribe()
        }
    }
    
//    func initSubscribeButton() {
//        let currentUserId = Auth.auth().currentUser?.uid ?? ""
//        FirebaseManager().getData(withKey: categoryID, equalToValue: currentUserId, completion: { (response) in
//            let subscribeModel = SubscribeModel(object: response)

//            if subscribeModel.status!, let btnTitle = self.btnFollow.titleLabel?.text, btnTitle == "Subscribe" {
//
//                self.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.9960784314, alpha: 1)
//                self.btnFollow.setTitle("Subscribed", for: .normal)
//                self.btnFollow.setTitleColor(#colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1), for: .normal)
//            }
//        })
//    }
    
    func enableSubscribe() {

        self.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        self.btnFollow.setTitle("Subscribed", for: .normal)
        self.btnFollow.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        FirebaseManager().addSubscribeCategory(withCategory: categoryID, status: true)
        
    }

    func disableSubscribe() {
        self.btnFollow.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.btnFollow.setTitle("Subscribe", for: .normal)
        self.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        FirebaseManager().addSubscribeCategory(withCategory: categoryID, status: false)
    }
    
}

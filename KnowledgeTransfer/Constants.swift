//
//  Constants.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/25/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

let MEMBER_KEY = "MEMBERS"
let INDICATOR_SIZE: CGFloat = 72.0
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let defaultDatePattern = "dd/MM/yyyy HH:mm"
let HTTP_CONTENTTYPE_FIELD = "Content-Type"
let HTTP_CONTENTTYPE_VALUE = "application/json"
let examLimits = [5,10,15]

enum ContextKey: String {
    case members = "MEMBERS"
    case contents = "CONTENTS"
    case categories = "CATEGORIES"
    case recents = "RECENTS"
    case subscribe = "SUBSCRIBE"
    case notification = "NOTIFICATIONS"
}

enum ContentStatus: String {
    case pub = "public"
    case pri = "private"
}

enum HTTPStatusCodes: Int {
    //Informational
    case Http100_Continue = 100
    case Http101_SwitchingProtocols = 101
    case Http102_Processing = 102
    //Success
    case Http200_OK = 200
    case Http201_Created = 201
    case Http202_Accepted = 202
    case Http203_NonAuthoritativeInformation = 203
    case Http204_NoContent = 204
    case Http205_ResetContent = 205
    case Http206_PartialContent = 206
    case Http207_MultiStatus = 207
    case Http208_AlreadyReported = 208
    case Http209_IMUsed = 209
    //Redirection
    case Http300_MultipleChoices = 300
    case Http301_MovedPermanently = 301
    case Http302_Found = 302
    case Http303_SeeOther = 303
    case Http304_NotModified = 304
    case Http305_UseProxy = 305
    case Http306_SwitchProxy = 306
    case Http307_TemporaryRedirect = 307
    case Http308_PermanentRedirect = 308
    //Client Error
    case Http400_BadRequest = 400
    case Http401_Unauthorized = 401
    case Http402_PaymentRequired = 402
    case Http403_Forbidden = 403
    case Http404_NotFound = 404
    case Http405_MethodNotAllowed = 405
    case Http406_NotAcceptable = 406
    case Http407_ProxyAuthenticationRequired = 407
    case Http408_RequestTimeout = 408
    case Http409_Conflict = 409
    case Http410_Gone = 410
    case Http411_LengthRequired = 411
    case Http412_PreconditionFailed = 412
    case Http413_RequestEntityTooLarge = 413
    case Http414_RequestURITooLong = 414
    case Http415_UnsupportedMediaType = 415
    case Http416_RequestedRangeNotSatisfiable = 416
    case Http417_ExpectationFailed = 417
    case Http418_IamATeapot = 418
    case Http419_AuthenticationTimeout = 419
    case Http420_MethodFailureSpringFramework_OR_EnhanceYourCalmTwitter = 420
    case Http422_UnprocessableEntity = 422
    case Http423_Locked = 423
    case Http424_FailedDependency_OR_MethodFailureWebDaw = 424
    case Http425_UnorderedCollection = 425
    case Http426_UpgradeRequired = 426
    case Http428_PreconditionRequired = 428
    case Http429_TooManyRequests = 429
    case Http431_RequestHeaderFieldsTooLarge = 431
    case Http444_NoResponseNginx = 444
    case Http449_RetryWithMicrosoft = 449
    case Http450_BlockedByWindowsParentalControls = 450
    case Http451_RedirectMicrosoft_OR_UnavailableForLegalReasons = 451
    case Http494_RequestHeaderTooLargeNginx = 494
    case Http495_CertErrorNginx = 495
    case Http496_NoCertNginx = 496
    case Http497_HTTPToHTTPSNginx = 497
    case Http499_ClientClosedRequestNginx = 499
    //Server Error
    case Http500_InternalServerError = 500
    case Http501_NotImplemented = 501
    case Http502_BadGateway = 502
    case Http503_ServiceUnavailable = 503
    case Http504_GatewayTimeout = 504
    case Http505_HTTPVersionNotSupported = 505
    case Http506_VariantAlsoNegotiates = 506
    case Http507_InsufficientStorage = 507
    case Http508_LoopDetected = 508
    case Http509_BandwidthLimitExceeded = 509
    case Http510_NotExtended = 510
    case Http511_NetworkAuthenticationRequired = 511
    case Http522_ConnectionTimedOut = 522
    case Http598_NetworkReadTimeoutErrorUnknown = 598
    case Http599_NetworkConnectTimeoutErrorUnknown = 599
    
    public var isInformational:Bool{
        return self.rawValue >= 100 && self.rawValue <= 199
    }
    
    public var isSuccess:Bool{
        return self.rawValue >= 200 && self.rawValue <= 299
    }
    
    public var isRedirection:Bool{
        return self.rawValue >= 300 && self.rawValue <= 399
    }
    
    public var isClientError:Bool{
        return self.rawValue >= 400 && self.rawValue <= 499
    }
    
    public var isServerError:Bool{
        return self.rawValue >= 500 && self.rawValue <= 599
    }
}

//
//  LoginViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/18/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase
import Material

class LoginViewController: UIViewController {

    @IBOutlet var textFieldUsername: TextField!
    @IBOutlet var textFieldPassword: TextField!

    @IBAction func loginDidTouch(_ sender: Any) {
        print("Login Did Touch")
    
        Auth.auth().signIn(withEmail: textFieldUsername.text!, password: textFieldPassword.text!, completion: { (user, error) in
            if user != nil {
                self.performSegue(withIdentifier: "loginSuccess", sender: nil)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != nil {
                print("User Auth Successful")
                self.performSegue(withIdentifier: "loginSuccess", sender: nil)
            }
        })
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "loginSuccess" || identifier == "register" {
            return true
        }
        
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


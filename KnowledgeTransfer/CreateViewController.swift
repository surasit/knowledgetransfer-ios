//
//  CreateViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/18/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CreateViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifierData = ["CellAddNew","CellExam","CellSetting"]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierData[indexPath.row], for: indexPath) as? CustomTableViewCell
        
        return cell!
    }
}

extension CreateViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}


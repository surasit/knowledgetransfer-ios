//
//  ReviewBoxViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 11/23/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit

class ReviewBoxViewController: UIViewController {

    @IBOutlet weak var reviewBoxView: ReviewBoxView!
    var contentModel: ContentModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        if let content = contentModel {
            reviewBoxView.contentModel = content
            reviewBoxView.reviewBoxViewController = self
            reviewBoxView.setupStartScore(content: content)
         }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  AccountViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/18/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase

class AccountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBAction func logoutDidTouch(_ sender: Any) {

        try? Auth.auth().signOut()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let frame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 150)
        let avatarView = AvatarView(frame: frame)
        
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableHeaderView = avatarView
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension AccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
}

extension AccountViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellBookmarks") as? AccountTableViewCell
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellBookmarks") as? AccountTableViewCell
        }else if indexPath.row == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellSetting") as? AccountTableViewCell
        }else if indexPath.row == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellAbout") as? AccountTableViewCell
        }
        
        cell?.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        
        return cell!
    }
}

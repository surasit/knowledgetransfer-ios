//
//  AddNewViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/26/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Material
import Firebase

class AddNewViewController: UIViewController {
    @IBOutlet var tfGroupName: TextField!
    @IBOutlet var tfTopicName: TextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tvObjective: TextView!
    
    var ref: DatabaseReference!
    let firebaseManager = FirebaseManager()
    var recentModel: [RecentModel]? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    let RECENT_WIDTH = 220.0
    let RECENT_HEIGHT = 100.0
    let minTextView = 20
    
    fileprivate lazy var loadingView: MaterialLoadingView = {
        let originX = SCREEN_WIDTH / 2.0
        var originY = SCREEN_HEIGHT / 2.0
        if let nav = self.navigationController {
            originY -= nav.navigationBar.frame.height
        }
        
        let loadingPoint = CGPoint(x: originX, y: originY)
        let indicator = MaterialLoadingView(frame: CGRect(x:originX - (INDICATOR_SIZE / 2), y: originY - (INDICATOR_SIZE / 2), width: INDICATOR_SIZE, height:INDICATOR_SIZE))
        indicator.center = loadingPoint
        self.view.addSubview(indicator)
        return indicator
    }()

    @IBAction func addPost(_ sender: UIButton) {
        
        self.loadingView.startLoading()
        
        guard tfGroupName.text != "",tfTopicName.text != "", tvObjective.text != "" else {
            self.loadingView.stopLoading()
            
            let alert = UIAlertController(title: "พบข้อผิดผลาล", message: "กรุณาตรวจสอบข้อมูลให้ครบถ้วน", preferredStyle: .alert)
            let action = UIAlertAction(title: "ปิด", style: .cancel, handler: nil)
            alert.addAction(action)
            
            self.navigationController?.present(alert, animated: true, completion: nil)
            
            return
        }
        
        if tvObjective.text.count < minTextView {
            self.loadingView.stopLoading()
            let alert = UIAlertController(title: "พบข้อผิดผลาล", message: "กรุณาระบุวัตถุประสงค์ของเนื้อหาให้ถูกต้อง \n(ขั้นต่ำ \(minTextView) ตัวอักษร)", preferredStyle: .alert)
            let action = UIAlertAction(title: "ปิด", style: .cancel, handler: nil)
            alert.addAction(action)
            
            self.navigationController?.present(alert, animated: true, completion: nil)
            return
        }

        self.readContent()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let userID = Auth.auth().currentUser?.uid else {
            return
        }
        
        firebaseManager.getAllRecents(withKey: "share_by", equalToValue: userID) { (response) in
            if let recent = response {
                self.recentModel = recent
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "createDetail", let content = sender {
            //Setup Contents Data Model
            let contentModel = ContentModel(object: content)
            
            let vc = segue.destination as? CreateDetailViewController
            vc?.contentModel = contentModel
        }

    }
}

extension AddNewViewController {
    func prepareWriteContent(params:[String:String]) {
        
        guard
            let groupName = params["category_name"],
            let topicName = params["topicname"]
        else {
            return
        }
        

//        let groupID = self.createGroupName(groupName: groupName)
        
        
        //Create Content
//        let contentPath = self.ref.child("CONTENTS")
//        let contentID = contentPath.childByAutoId()
        
        let contentData = ["objective": params["objective"],"content_id":"","topic_name":topicName,"cat_id":"","cat_name":groupName,"description":"","status":"public"]
        
        //Add Topic name then can show list on home page
//        contentID.setValue(contentData)

        self.loadingView.stopLoading()
        self.performSegue(withIdentifier: "createDetail", sender: contentData)
        
    }
    
    func createGroupName(groupName:String) -> String {
        //Add allow user id access into this group
        
        let groupRef = self.ref.child("CATEGORIES")
        let groupChild = groupRef.childByAutoId()
        
        var groupID = groupChild.key
        
        groupRef.observeSingleEvent(of: .value, with: { (snapshot) in

            guard let groupLists = snapshot.value as? [String:AnyObject] else {
                let groupInfo = ["id":groupID,"name":groupName]
                groupChild.setValue(groupInfo)
                
                return
            }

            for (_,groupValue) in groupLists {
                if let gName = groupValue["name"] as? String, let gID = groupValue["id"] as? String, gName == groupName {
                    groupID = gID
                    return
                }
            }
            
            let groupInfo = ["id":groupID,"name":groupName]
            groupChild.setValue(groupInfo)
        })
        
        return groupID
        
    }
    
    func readContent() {
        guard let userID = Auth.auth().currentUser?.uid else {
            return
        }
        
        self.ref.child(MEMBER_KEY).child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let params = ["category_name":self.tfGroupName.text!,"topicname":self.tfTopicName.text!,"useraccess":userID, "objective":self.tvObjective.text!]
            self.prepareWriteContent(params: params)

        })
    }
    
}

extension AddNewViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = CGSize(width: RECENT_WIDTH, height: RECENT_HEIGHT)
        var elementSize = CGSize.zero
        if let recent = self.recentModel, !recent.isEmpty {
            elementSize = itemSize
        }

        return elementSize
    }
}

extension AddNewViewController: UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Recent did Select at index : \(indexPath)")
        
        if let recentModel = self.recentModel {
            self.tfGroupName.text = recentModel[indexPath.row].catName
        }
        
    }
}

extension AddNewViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRecent", for: indexPath) as? CustomCollectionViewCell
        //cell?.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        cell?.setBackgroundBlur()
        if let recentModel = self.recentModel {
            cell?.setupRecentCell(withModel: recentModel[indexPath.row])
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recentModel?.count ?? 0
    }
    
}

//
//  MaterialLoadingView.swift
//  V1
//
//  Created by Surasit Intawong on 5/17/2560 BE.
//  Copyright © 2560 Ascend Money Thailand. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner

class MaterialLoadingView: UIView {
    
    @IBOutlet var loadingIndicator: MMMaterialDesignSpinner!
    
    init() {
        super.init(frame: CGRect())
        fromNib()
    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        fromNib()
    }
    
    func setupFrame(frame: CGRect) {
        self.frame = frame
    }
    
    func startLoading() {
        loadingIndicator.startAnimating()
    }
    
    func stopLoading() {
        loadingIndicator.stopAnimating()
        self.removeFromSuperview()
    }
}


//
//  NotificationViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 6/4/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class NotificationViewController: UIViewController {
   
    lazy var loadingView: MaterialLoadingView = {
        let loadingIndicator = MaterialLoadingView(frame: self.view.frame)
        loadingIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(loadingIndicator)
        return loadingIndicator
    }()
    let errorReponseView = ErrorResponseView()
    var contentLists: [ContentModel]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    let firebaseManager = FirebaseManager()
    var handle: AuthStateDidChangeListenerHandle?
    
    let reuseCellIndentifierNotification = "CellNotifications"
    var ref: DatabaseReference!
    var jsonData: JSON!
    
    @IBOutlet weak var tableView: UITableView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getNotificationList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            
            guard let content = self.contentLists, let indexPath = self.tableView.indexPathForSelectedRow else {
                return
            }
            
            let detailVC = segue.destination as? DetailViewController
            detailVC?.contentModel = content[indexPath.row]
            detailVC?.hidesBottomBarWhenPushed = true
        }
    }
 

}

extension NotificationViewController {
    func getNotificationList() {
        //let currentUserId = Auth.auth().currentUser?.uid ?? ""
        firebaseManager.getAllNotification(completion: { (contents) in
            guard let content = contents else {
                //Is empty data should be show header empty view in tableViewHeader
                self.errorReponseView.setViewStatus(status: .Http204_NoContent, andTitle: "", andSubTitle: "Contents_Empty".localized)
                self.tableView.setTableHeaderViewStatus(in: self.tableView, responseView: self.errorReponseView)
                
                return
            }
            
//            for contentList in content {
//                FirebaseManager().getData(withKey: contentList.catId ?? "", equalToValue: currentUserId, completion: { (categoryResponse) in
//                    let subscribeModel = SubscribeModel(object: categoryResponse)
//                    if let subscribeUserId = subscribeModel.userId, subscribeUserId == currentUserId {
//                        print("user match this category")
//                    }
//                })
//            }
            
            self.contentLists = content
            self.tableView.isHidden = false
            self.tableView.tableHeaderView = nil
            self.loadingView.stopLoading()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.tableView.reloadData()
            }
        }) { (error) in
            print("Empty Knowledge lists")
            self.loadingView.stopLoading()
            self.errorReponseView.setViewStatus(status: .Http204_NoContent, andTitle: "", andSubTitle: "Contents_Empty".localized)
            self.tableView.setTableHeaderViewStatus(in: self.tableView, responseView: self.errorReponseView)
        }
        
    }
}

extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.rowHeight
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}

extension NotificationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contentLists != nil ? self.contentLists!.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIndentifierNotification) as? CustomTableViewCell

        if let content = self.contentLists {
            cell?.setupNotificationTopic(withTopic: content[indexPath.row])
        }

        
        return cell!
    }

}

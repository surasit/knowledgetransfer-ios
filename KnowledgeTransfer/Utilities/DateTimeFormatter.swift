//
//  DateTimeFormatter.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 7/2/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import Foundation

class DateTimeFormatter: NSObject {
    
    func getTimeInterval() -> Int {
        // using current date and time as an example
        let someDate = Date()
        
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = someDate.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        
        return myInt
    }

}

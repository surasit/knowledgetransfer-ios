//
//  CreateDetailViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/27/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQKeyboardManager
import Material
import Firebase
import UserNotifications
import RichEditorView

class CreateDetailViewController: UIViewController {
    
    let user = Auth.auth().currentUser
    var ref: DatabaseReference!
    var contentModel: ContentModel!
    let contentManager = ContentManager()
    var thumbnail: String? = nil
    
    lazy var toolbar: RichEditorToolbar = {
        let options: [RichEditorDefaultOption] = [.image, .alignLeft,.alignCenter,.alignRight,.unorderedList,.link]
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = options
        return toolbar
    }()
    
    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var txtViewDetail: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBAction func saveDidTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        var options = toolbar.options
        options.append(item)
        toolbar.options = options
        
//        UNUserNotificationCenter.current().requestAuthorization(
//            options: [.alert,.sound,.badge],
//            completionHandler: { (granted,error) in
//                self.isGrantedNotificationAccess = granted
//        }
//        )
        
        let nextButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextViewDidTouch))
        self.navigationItem.rightBarButtonItem = nextButton

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nextViewDidTouch() {
        contentModel.descriptionValue = editorView.contentHTML
        if let img = thumbnail {
            contentModel.thumbnail = img
        }
        
        if let user = user {

            contentModel.authorId = user.uid
            contentModel.author = user.displayName
            
            let timestamp = Date()
            contentModel.created = timestamp.toString(with: defaultDatePattern)
            
            if let authorAvatar = user.photoURL {
                contentModel.authorAvatar = "\(authorAvatar)"
            }
            
            contentManager.writeContent(params: contentModel, atPath: ContextKey.contents.rawValue, success: { (response) in
                print("Save Content Success")
                
                NotificationManager().send(withTopic: self.contentModel)
                
                self.navigationController?.dismiss(animated: true, completion: nil)
            }) { (error) in
                print("Save Content Error")
                print(error)
            }
        }

    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "createDetail" {
            return true
        }
        
        return false
    }

     //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createDetail" {
            let _ = segue.destination as? CreateDetailViewController
        }
    }
    
    func uploadImageToFirebaseStorage(data:NSData){
        //TODO: Make this work
        
        let storageManager = StorageManager(withBucketName: .content)
        storageManager.uploadContentImage(imageData: data, completion: { (imageURL, fileName) in
            if self.thumbnail == nil {
                self.thumbnail = "\(imageURL)"
            }
            self.toolbar.editor?.insertImage("\(imageURL)", alt: fileName)
        }) { (error) in
            print("Upload Image Error : \(error.localizedDescription)")
        }
        
//        let timeInterval = DateTimeFormatter().getTimeInterval()
//
//        let storageRef = Storage.storage().reference(withPath: "content/\(timeInterval)")
//        let uploadMetaData = StorageMetadata()
//        uploadMetaData.contentType = "image/jpeg"
//        let uploadTask = storageRef.putData(data as Data, metadata: uploadMetaData) { (metadata, error) in
//            if (error != nil) {
//                print("I received an error! \(String(describing: error?.localizedDescription))")
//            }else{
//                guard let metaDataInfo = metadata, let downloadURL = metaDataInfo.downloadURL() else {
//                    return
//                }
//                print("Upload complete metadata >>  \(metaDataInfo)")
//                print("Download URL : \(downloadURL)")
//                if self.thumbnail == nil {
//                    self.thumbnail = "\(downloadURL)"
//                }
//                self.toolbar.editor?.insertImage("\(downloadURL)", alt: "filename_\(timeInterval)")
//            }
//        }
//
//        // Update ther progress bar
//        uploadTask.observe(.progress) { [weak self]  (snapshot) in
//            guard let _ = self else { return }
//            guard let progress = snapshot.progress else { return }
//            print(progress)
//        }

    }
    
    func uploadMovieToFirebaseStorage(url:NSURL) {
        //TODO: Make this work
    }

}

extension CreateDetailViewController: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        
        if content.isEmpty {
            //txtViewDetail.text = "HTML Preview"
        } else {
            //txtViewDetail.text = content
        }
    }
    
}

extension CreateDetailViewController: RichEditorToolbarDelegate {
    

    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        print("ImagePicker")
        
        uploadButtonWasPressed()
        
        //toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if toolbar.editor?.hasRangeSelection == true {
            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
        }
    }
}

extension CreateDetailViewController: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType: String = info[UIImagePickerControllerMediaType] as? String else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        if mediaType == (kUTTypeImage as String) {
            //The user has selected an image
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                guard let imgNew = originalImage.resizeImage(image: originalImage, newWidth: 300), let imageData = UIImageJPEGRepresentation(imgNew, 0.8) else {
                    return
                }
                
                uploadImageToFirebaseStorage(data: imageData as NSData)
            }
        }else if mediaType == (kUTTypeMovie as String) {
            if let movieURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                uploadMovieToFirebaseStorage(url: movieURL)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func uploadButtonWasPressed() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    
}

extension CreateDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("Should Return")
        return true
    }
}


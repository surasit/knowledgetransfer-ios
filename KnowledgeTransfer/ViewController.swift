//
//  ViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/14/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class ViewController: UIViewController {
    
    lazy var loadingView: MaterialLoadingView = {
        let loadingIndicator = MaterialLoadingView(frame: self.view.frame)
        loadingIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(loadingIndicator)
        return loadingIndicator
    }()
    let errorReponseView = ErrorResponseView()
    var contentLists: [ContentModel]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    let firebaseManager = FirebaseManager()
    var handle: AuthStateDidChangeListenerHandle?
    
    @IBOutlet var tableView: UITableView!
    @IBAction func createView(_ sender: Any) {
        let createVC = UIStoryboard(name: "Create", bundle: nil).instantiateInitialViewController()
        self.navigationController?.present(createVC!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 200.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadingView.startLoading()
        
        firebaseManager.getAllContents(completion: { (contents) in
            guard let content = contents else {
                //Is empty data should be show header empty view in tableViewHeader
                self.errorReponseView.setViewStatus(status: .Http204_NoContent, andTitle: "", andSubTitle: "Contents_Empty".localized)
                self.tableView.setTableHeaderViewStatus(in: self.tableView, responseView: self.errorReponseView)
                
                return
            }
            
            self.contentLists = content.reversed()
            self.tableView.isHidden = false
            self.tableView.tableHeaderView = nil
            self.loadingView.stopLoading()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.tableView.reloadData()
            }
        }) { (error) in
            print("Empty Knowledge lists")
                self.loadingView.stopLoading()
                self.errorReponseView.setViewStatus(status: .Http204_NoContent, andTitle: "", andSubTitle: "Contents_Empty".localized)
                self.tableView.setTableHeaderViewStatus(in: self.tableView, responseView: self.errorReponseView)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {

            guard let content = self.contentLists, let indexPath = self.tableView.indexPathForSelectedRow else {
                return
            }
            
            let detailVC = segue.destination as? DetailViewController
            detailVC?.contentModel = content[indexPath.row]
            detailVC?.hidesBottomBarWhenPushed = true
        }
    }
    
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}
extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contentLists != nil ? self.contentLists!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellHome"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CustomTableViewCell
        
        if let content = self.contentLists {
            cell?.setupTopic(withTopic: content[indexPath.row])
        }

        return cell!
    }
    
}

//
//  DetailViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 6/24/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import FBSDKShareKit
import Firebase
import Material

class DetailViewController: UIViewController {

    let firebaseManager = FirebaseManager()
    var contentModel: ContentModel?
    var increaseView = false
    var increaseShare = false
    let screenHeight: CGFloat = ScreenSize.SCREEN_HEIGHT
    static let DYNAMIC_LINK_DOMAIN = "zm37s.app.goo.gl"
    
    var currentUserId = ""
    var contentId = ""
    var reviewedKey = ""
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnExam: Button!
    
    @IBAction func takeExamDidTouch(_ sender: Button) {
        print("Take Exam")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bookmarkButton = UIBarButtonItem(image: UIImage(named:"ic_bookmark_border"), style: .plain, target: self, action: #selector(showActionSheet))
        let shareButton = UIBarButtonItem(image: UIImage(named:"ic_share"), style: .plain, target: self, action: #selector(showActionSheet))
        navigationItem.rightBarButtonItems = [shareButton, bookmarkButton]

        guard let content = contentModel else {
            return
        }
        
        self.title = content.topicName
        
        self.loadHeader()
        self.loadHTML()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let content = contentModel {
            currentUserId = Auth.auth().currentUser?.uid ?? ""
            contentId = content.contentId ?? ""
            reviewedKey = "\(currentUserId)_\(contentId)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func loadHeader() {
        guard let contents = contentModel else {
            return
        }
        
        guard let imgURL = contents.authorAvatar else {
            return
        }
        
        self.headerView.contentModel = contents
        self.headerView.setAvatar(withImageURL: imgURL)
        self.headerView.setupInformation(contents: contents)

    }
    
    fileprivate func loadHTML() {
        
        guard let content = contentModel,let topic = content.topicName, let desc = content.descriptionValue else {
            return
        }
        
        let objectiveContent = "<strong>วัตถุประสงค์ของเนื้อหา</strong><br> \(content.objective ?? "")"
        
        let styleCSS = "<style type=\"text/css\">body{font-family:sans-serif;margin:0px;padding-bottom:8px;font-size:%fpx;}img{display:block;width:auto!important;height:auto!important;max-width:320px!important;margin-left:auto;margin-right:auto;padding:8px 0px;} iframe{width:auto;height:auto;padding:8px 0px;max-width:320px;margin-left:auto;margin-right:auto;} header { height:auto;font-weight:bold; background-color:#fff;color:#4a4a4a;line-height:1.0;margin-left:8px;margin-right:8px;font-size:2.0em;padding-top:8px;padding-bottom:16px;}h2,h2.topic-native { padding:10px;font-size:1.3em;}div.native {margin:0px;color:#5e5e5e;margin-left:8px;margin-right:8px;}div.native p {line-height:1.5; margin-bottom:10px!important;margin-left:8px;margin-right:8px} a { text-decoration:none; }</style>"
        let body = "<html><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\">\(styleCSS)<body><header>\(topic)</header><div class=\"native\"><p>\(objectiveContent)<p>\(desc)</div></body></html>"
        
        //self.webView.scrollView.contentInset.bottom = 50
        self.webView.loadHTMLString(body as String, baseURL: nil)
        self.webView.scrollView.delegate = self
    }
    
    func showActionSheet() {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Share with", preferredStyle: .actionSheet)
        
        // 2
        let fbShareAction = UIAlertAction(title: "Facebook", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.shareFacebook()
        })
//        let lineShareAction = UIAlertAction(title: "Line", style: .default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.shareLine()
//        })
//        
//        let emailShareAction = UIAlertAction(title: "E-Mail", style: .default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.shareEmail()
//        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(fbShareAction)
//        optionMenu.addAction(lineShareAction)
//        optionMenu.addAction(emailShareAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func shareFacebook() {
        print("Share with Facebook")
        
        guard let content = contentModel,let topic = content.topicName else {
            return
        }

        guard let link = URL(string: "https://zm37s.app.goo.gl/qbvQ") else { return }
        let components = DynamicLinkComponents(link: link, domain: DetailViewController.DYNAMIC_LINK_DOMAIN)
        
        let socialParams = DynamicLinkSocialMetaTagParameters()
        socialParams.title = topic
        
        if let writer = content.author {
            socialParams.descriptionText = "เขียนโดย : \(writer)"
        }
        
        if let imageURL = content.thumbnail {
            socialParams.imageURL = URL(string: imageURL)
        }
        components.socialMetaTagParameters = socialParams

        let options = DynamicLinkComponentsOptions()
        options.pathLength = .unguessable
        components.options = options
        components.shorten { (shortURL, warnings, error) in
            // Handle shortURL.
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let shortLink = shortURL {
                print("CanShare This Short Link")
                print(shortLink.absoluteString)
                
                //Example for share link content dialog
                let content = FBSDKShareLinkContent()
                content.contentURL = shortLink
                
                let dialog = FBSDKShareDialog()
                dialog.fromViewController = self
                dialog.shareContent = content
                if dialog.canShow() {
                    dialog.mode = .automatic
                }else{
                    dialog.mode = .feedBrowser
                }
                dialog.show()
                
                if let content = self.contentModel {
                    self.firebaseManager.addShareCounter(withContent: content) { (status) in
                        print("Counter Shared status : \(status)")
                    }
                }
            }
        }
        
    }
    
    func shareLine() {
        print("Share with Line")
    }
    
    func shareEmail() {
        print("Share by Email")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exam" {
            let examVC = segue.destination as? ExamViewController
            examVC?.contentModel = contentModel
        }
    }
}

extension DetailViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("webViewDidStart")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinish")
        
        let contentHeight = self.webView.scrollView.contentSize.height
        if screenHeight > contentHeight {
            if let content = contentModel {
                self.firebaseManager.addViewCounter(withContent: content) { (status) in
                    print("Counter status : \(true)")
                    self.increaseView = true
                }
                
                if UserDefaults.standard.bool(forKey: reviewedKey) != true {
                    UserDefaults.standard.set(true, forKey: reviewedKey)
                    //Show Review box
                    print("Show review box")
                    if let reviewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reviewBoxViewController") as? ReviewBoxViewController {
                        reviewVC.contentModel = contentModel
                        self.present(reviewVC, animated: true, completion: nil)
                    }
                }
            }
        }

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("webViewDidFail")
    }
}

extension DetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentHeight = self.webView.scrollView.contentSize.height
        
        if screenHeight < contentHeight, screenHeight < scrollView.contentOffset.y {
            if let content = contentModel, increaseView == false {
                self.firebaseManager.addViewCounter(withContent: content) { (status) in
                    print("Scroll Counter status : \(true)")
                    self.increaseView = true
                }
                
                if UserDefaults.standard.bool(forKey: reviewedKey) != true {
                    UserDefaults.standard.set(true, forKey: reviewedKey)
                    print("Show review box")
                    if let reviewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reviewBoxViewController") as? ReviewBoxViewController {
                        reviewVC.contentModel = contentModel
                        self.present(reviewVC, animated: true, completion: nil)
                    }
                }
                
            }
        }
    }
}

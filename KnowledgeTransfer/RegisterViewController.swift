//
//  RegisterViewController.swift
//  KnowledgeTransfer
//
//  Created by Surasit Intawong on 4/25/2560 BE.
//  Copyright © 2560 Intawong. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Material

class RegisterViewController: UIViewController {
    var ref: DatabaseReference!

    @IBOutlet var tfFullName: TextField!
    @IBOutlet var tfUsername: TextField!
    @IBOutlet var tfPassword: TextField!
    @IBOutlet var imgDisplayProfile: UIImageView!
    
    @IBAction func dismissDidTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerDidTouch(_ sender: Any) {
        print("Register Save")
        
        guard tfFullName.text != "",tfUsername.text != "", tfPassword.text != "" else {
            return
        }

        let params = ["username":tfUsername.text!,"password":tfPassword.text!,"fullname":tfFullName.text!]
        
        let memberProfileModel = MemberProfileModel(object: params)
        
        writeDataProfile(withModel: memberProfileModel)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        
        let displayGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView))
        imgDisplayProfile.isUserInteractionEnabled = true
        imgDisplayProfile.addGestureRecognizer(displayGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "home" {
            return true
        }
        
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "home" {
            let _ = segue.destination as? ViewController
        }
    }

}

extension RegisterViewController {
    func writeDataProfile(withModel memberProfileModel: MemberProfileModel) {
        Auth.auth().createUser(withEmail: memberProfileModel.username ?? "", password: memberProfileModel.password ?? "", completion: { (user, error) in
            if error != nil {
                print("Error is : \(String(describing: error))")
            }else{
                guard let userID = user?.uid else {
                    return
                }
                
                //Update member profile request by auth service
                memberProfileModel.userid = userID
                self.updateProfile(memberProfileModel: memberProfileModel)
                
            }
        })

    }
    
    func updateProfile(memberProfileModel: MemberProfileModel) {

        //Update member profile avatar image default and also ramdom to storage
        let storageManager = StorageManager(withBucketName: .defaultAvatar)
        storageManager.loadAvatarImage(completion: { (imageURL) in
            
            if let imgURL = imageURL {
                memberProfileModel.avatar = "\(imgURL)"
            }
            
            //Update member profile to realtime database
            self.ref.child(MEMBER_KEY).child(memberProfileModel.userid ?? "").setValue(memberProfileModel.dictionaryRepresentation())
            
            let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
            changeRequest?.displayName = memberProfileModel.fullname
            changeRequest?.photoURL = imageURL
            changeRequest?.commitChanges(completion: { (error) in
                if error != nil {
                    print("Update Profile Error")
                    print(error?.localizedDescription ?? "")
                }
            })
            
            self.performSegue(withIdentifier: "home", sender: nil)
        }) { (error) in
            print(error ?? "")
        }
        

    }
    
    func readDataProfile() {
        guard let userID = Auth.auth().currentUser?.uid else {
            return
        }
        
        self.ref.child(MEMBER_KEY).child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? [String:AnyObject]
            let username = value?["username"] as? String
            //print(value)
            print("UserName : \(String(describing: username))")
        })
    }

}

extension RegisterViewController: UIImagePickerControllerDelegate {
    func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
}

//
//  SubscribeModel.swift
//
//  Created by Surasit Intawong on 11/12/2560 BE
//  Copyright (c) . All rights reserved.
//

import Foundation

public class SubscribeModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fcmToken = "fcm_token"
    static let status = "status"
  }

  // MARK: Properties
  public var fcmToken: String?
  public var status: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    fcmToken = json[SerializationKeys.fcmToken].string
    status = json[SerializationKeys.status].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fcmToken { dictionary[SerializationKeys.fcmToken] = value }
    dictionary[SerializationKeys.status] = status
    return dictionary
  }

}
